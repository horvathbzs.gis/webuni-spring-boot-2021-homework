package hu.webuni.horvathbzs.hr.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

import hu.webuni.horvathbzs.hr.model.Company;
import hu.webuni.horvathbzs.hr.model.Employee;
import hu.webuni.horvathbzs.hr.repository.CompanyRepository;
import hu.webuni.horvathbzs.hr.repository.EmployeeRepository;

@SpringBootTest
@AutoConfigureTestDatabase
public class CompanyServiceIT {

	@Autowired
	CompanyService companyService;

	@Autowired
	CompanyRepository companyRepository;

	@Autowired
	EmployeeRepository employeeRepository;

	/*
	 * This method runs before each test method to clear the database
	 */
	@BeforeEach
	public void init() {
		employeeRepository.deleteAll();
		companyRepository.deleteAll();
	}

	/*
	 * Creates a Test Company
	 * 
	 * @return Company id
	 */
	private long createCompany(int registrationNumber, String name) {
		return companyService.save(new Company(registrationNumber, name)).getId();
	}

	/*
	 * Adds a test Employee to the Test Company
	 * 
	 * @return Employee id
	 */
	private long addEmployeeToCompany(long companyId, String name, int salary, LocalDateTime entryDate) {
		companyService.addEmployee(companyId, new Employee(name, salary, entryDate));
		Company company = companyRepository.findById(companyId).get();
		Employee employee = company.getEmployees().get(0);
		long employeeId = employee.getId();
		// long employeeId =
		// companyRepository.findById(companyId).get().getEmployeeList().get(0).getId();
		return employeeId;
	}

	/*
	 * Tests whether employee has been added to a company.
	 * 
	 */
	@Test
	void testAddEmployee() throws Exception {
		long companyId = createCompany(123, "Test Company");
		String employeeName = "Test Employee";
		long employeeId = addEmployeeToCompany(companyId, employeeName, 1000, LocalDateTime.now().minusDays(1));

		Optional<Employee> addedEmployeeOptional = employeeRepository.findById(employeeId);
		assertThat(addedEmployeeOptional).isNotEmpty();
		assertThat(addedEmployeeOptional.get().getCompany().getId()).isEqualTo(companyId);
		assertThat(addedEmployeeOptional.get().getName()).isEqualTo(employeeName);
	}

	/*
	 * Tests whether employee's contact attribute is null, and the employee has been
	 * removed from to a company's employeeList.
	 * 
	 */
	@Test
	void testDeleteEmployee() throws Exception {
		long companyId = createCompany(123, "Test Company");
		String employeeName = "Test Employee";
		long employeeId = addEmployeeToCompany(companyId, employeeName, 1000, LocalDateTime.now().minusDays(1));

		companyService.deleteEmployee(companyId, employeeId);

		Optional<Company> companyOptinal = companyRepository.findById(companyId);
		assertThat(companyOptinal.get().getEmployees().size()).isEqualTo(0);

		Optional<Employee> employeeDeletedFromCompanyOptional = employeeRepository.findById(employeeId);
		assertThat(employeeDeletedFromCompanyOptional.get().getCompany()).isNull();
	}

	@Test
	void testReplaceEmployees() throws Exception {
		long companyId = createCompany(123, "Test Company");
		String employeeName = "Test Employee";
		long employeeId = addEmployeeToCompany(companyId, employeeName, 1000, LocalDateTime.now().minusDays(1));

		String replaceEmployeeName = "Modified Employee";
		Employee replaceEmployee = new Employee(replaceEmployeeName, 1000, LocalDateTime.now());
		List<Employee> replaceEmployeeList = new ArrayList<>();
		replaceEmployeeList.add(replaceEmployee);

		companyService.replaceEmployees(companyId, replaceEmployeeList);

		Optional<Employee> originalEmployeeOptional = employeeRepository.findById(employeeId);
		Optional<Employee> replacedEmployeeOptional = employeeRepository.findById(replaceEmployee.getId());
		assertThat(replacedEmployeeOptional).isNotEmpty();
		assertThat(replacedEmployeeOptional.get().getCompany().getId()).isEqualTo(companyId);
		assertThat(replacedEmployeeOptional.get().getName()).isEqualTo(replaceEmployeeName);
		assertThat(originalEmployeeOptional.get().getCompany()).isNull();
	}
}
