package hu.webuni.horvathbzs.hr.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import hu.webuni.horvathbzs.hr.dto.HolidayRequestFilterDto;
import hu.webuni.horvathbzs.hr.model.Employee;
import hu.webuni.horvathbzs.hr.model.HolidayRequest;
import hu.webuni.horvathbzs.hr.repository.EmployeeRepository;
import hu.webuni.horvathbzs.hr.repository.HolidayRequestRepository;

@SpringBootTest
@AutoConfigureTestDatabase
public class HolidayRequestServiceIT {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	HolidayRequestRepository holidayRequestRepository;

	@Autowired
	HolidayRequestService holidayRequestService;

	@BeforeEach
	public void init() {
		holidayRequestRepository.deleteAll();
		employeeRepository.deleteAll();
	}

	/*
	 * Test method for holidayRequestService.addHolidayRequest(), asserts that the
	 * database contains HolidayRequest record, asserts that the HolidayRequest
	 * record's employee is the one that was added as the service method's parameter
	 */
	@Test
	void testAddHolidayRequest() throws Exception {

		long employeeId = initEmployee("Test Employee");
		HolidayRequest request = createHolidayRequest(LocalDate.now().plusDays(3), LocalDate.now().plusDays(5));
		
		// Employee employee = employeeRepository.findById(employeeId).get();
		HolidayRequest savedRequest = holidayRequestService.addHolidayRequest(request, employeeId);
		assertThat(savedRequest).isNotNull();
		assertThat(savedRequest.getEmployee().getId()).isEqualTo(employeeId);
		// assertThat(employee.getHolidayRequests().get(0)).isEqualTo(requests.get(0));
	}

	/*
	 * Test method for holidayRequestService.approveHolidayRequest()
	 * Manager denies the request: parameter value false
	 */
	@Test
	void testDenyHolidayRequest() throws Exception {

		long employeeId = initEmployee("Test Employee");
		long managerId = initEmployee("Test Manager");

		HolidayRequest request = createHolidayRequest(LocalDate.now().plusDays(3), LocalDate.now().plusDays(5));
		long requestId = holidayRequestService.addHolidayRequest(request, employeeId).getId();

		HolidayRequest deniedRequest = holidayRequestService.approveHolidayRequest(requestId, managerId, false);
		assertThat(deniedRequest).isNotNull();
		assertThat(deniedRequest.getEmployee().getId()).isEqualTo(employeeId);
		assertThat(deniedRequest.getApprover().getId()).isEqualTo(managerId);
		assertThat(deniedRequest.getApproved()).isFalse();
	}
	
	/*
	 * Test method for holidayRequestService.approveHolidayRequest()
	 * Manager approves the request: parameter value true
	 */
	@Test
	void testAproveHolidayRequest() throws Exception {

		long employeeId = initEmployee("Test Employee");
		long managerId = initEmployee("Test Manager");

		HolidayRequest request = createHolidayRequest(LocalDate.now().plusDays(3), LocalDate.now().plusDays(5));
		long requestId = holidayRequestService.addHolidayRequest(request, employeeId).getId();

		HolidayRequest aprovedRequest = holidayRequestService.approveHolidayRequest(requestId, managerId, true);
		assertThat(aprovedRequest).isNotNull();
		assertThat(aprovedRequest.getEmployee().getId()).isEqualTo(employeeId);
		assertThat(aprovedRequest.getApprover().getId()).isEqualTo(managerId);
		assertThat(aprovedRequest.getApproved()).isTrue();
	}
	
	/*
	 * Test method for holidayRequestService.modifyHolidayRequest()
	 * Testing that the end date of the request has been modified.
	 */
	@Test
	void testModifyHolidayRequest() throws Exception {
		long employeeId = initEmployee("Test Employee");
		HolidayRequest request = createHolidayRequest(LocalDate.now().plusDays(3), LocalDate.now().plusDays(5));
		HolidayRequest newRequest = createHolidayRequest(LocalDate.now().plusDays(3), LocalDate.now().plusDays(10));
		
		HolidayRequest savedRequest = holidayRequestService.addHolidayRequest(request, employeeId);
		HolidayRequest modifiedRequest = holidayRequestService.modifyHolidayRequest(savedRequest.getId(), newRequest);
		assertThat(savedRequest.getId()).isEqualTo(modifiedRequest.getId());
		assertThat(modifiedRequest.getEndDate()).isNotEqualTo(savedRequest.getEndDate());
	}
	
	/*
	 * Test method for holidayRequestService.deleteHolidayRequest()
	 */
	@Test
	void testDeleteHolidayRequest() throws Exception {
		long employeeId = initEmployee("Test Employee");
		HolidayRequest request = createHolidayRequest(LocalDate.now().plusDays(3), LocalDate.now().plusDays(5));	
		
		long requestId = holidayRequestService.addHolidayRequest(request, employeeId).getId();
		holidayRequestService.deleteHolidayRequest(requestId);
		Optional<HolidayRequest> deletedRequestOptional = holidayRequestRepository.findById(requestId);
		assertThat(deletedRequestOptional).isEmpty();
	}
	
	@Test
	void testFindHolidayRequestsByExample() throws Exception {		
		LocalDate startHoliday = LocalDate.now().plusDays(3);
		LocalDate endHoliday = LocalDate.now().plusDays(5);
		
		long employeeId = initEmployee("Test Employee");
		long managerId = initEmployee("Test Manager");
		HolidayRequest request = createHolidayRequest(startHoliday, endHoliday);
		long requestId = holidayRequestService.addHolidayRequest(request, employeeId).getId();
		HolidayRequest aprovedRequest = holidayRequestService.approveHolidayRequest(requestId, managerId, true);
		
		HolidayRequestFilterDto example = new HolidayRequestFilterDto();
		example.setEmployeeName("Test Employee");
		example.setApproverName("Test Manager");
		example.setApproved(true);
		example.setStartDate(startHoliday.minusDays(1));
		example.setEndDate(endHoliday.plusDays(1));
		example.setCreateDateTimeStart(LocalDateTime.now().minusDays(1));
		example.setCreateDateTimeEnd(LocalDateTime.now().plusDays(1));
		
		Pageable pageable = PageRequest.of(0, 10);
		List<HolidayRequest> foundRequests = this.holidayRequestService.findHolidayRequestsByExample(example, pageable).getContent();
		assertThat(foundRequests).isNotEmpty();
	}

	private long initEmployee(String name) {
		return employeeRepository.save(new Employee(name)).getId();
	}

	private HolidayRequest createHolidayRequest(LocalDate startDate, LocalDate endDate) {
		HolidayRequest request = new HolidayRequest();
		request.setStartDate(startDate);
		request.setEndDate(endDate);

		return request;
	}

}
