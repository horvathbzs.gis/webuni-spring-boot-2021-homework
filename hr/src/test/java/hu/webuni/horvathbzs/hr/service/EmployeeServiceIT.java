package hu.webuni.horvathbzs.hr.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

import hu.webuni.horvathbzs.hr.model.Company;
import hu.webuni.horvathbzs.hr.model.Employee;
import hu.webuni.horvathbzs.hr.model.Position;
import hu.webuni.horvathbzs.hr.model.Qualification;
import hu.webuni.horvathbzs.hr.repository.CompanyRepository;
import hu.webuni.horvathbzs.hr.repository.EmployeeRepository;
import hu.webuni.horvathbzs.hr.repository.PositionRepository;

@SpringBootTest
@AutoConfigureTestDatabase
public class EmployeeServiceIT {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	CompanyRepository companyRepository;

	@Autowired
	PositionRepository positionRepository;

	@Autowired
	AbstractEmployeeService employeeService;

	@Autowired
	CompanyService companyService;

	@BeforeEach
	public void init() {
		positionRepository.deleteAll();
		employeeRepository.deleteAll();
		companyRepository.deleteAll();
	}

	private int createPosition(String name, Qualification qualification) {
		return positionRepository.save(new Position(name, qualification)).getId();
	}

	private long createEmployee(String name, Position position, int salary, LocalDateTime entryDate) {
		return employeeRepository.save(new Employee(name, position, salary, entryDate)).getId();
	}

	private long createCompany(int registrationNumber, String name) {
		return companyRepository.save(new Company(registrationNumber, name)).getId();
	}

	@Test
	void testFindEmployeesByExample() throws Exception {
		
		System.out.println(" EmployeeServiceIT > testFindEmployeesByExample() > START");
		
		int positionId = createPosition("Test Position", Qualification.UNIVERSITY);
		System.out.println("EmployeeServiceIT > testFindEmployeesByExample() > positionId: " + positionId);
		Position position = positionRepository.findById(positionId).get();
		
		long company1Id = createCompany(123, "Test Company");
		long company2Id = createCompany(124, "TEST COMPANY");

		int salary = 1000;
		long employee1Id = createEmployee("Test Employee", position, salary, LocalDateTime.now().minusDays(5));
		long employee2Id = createEmployee("TEST EMPLOYEE", position, salary, LocalDateTime.now().minusDays(5));
		Employee employee1 = employeeRepository.findById(employee1Id).get();
		Employee employee2 = employeeRepository.findById(employee2Id).get();

		companyService.addEmployee(company1Id, employee1);
		companyService.addEmployee(company2Id, employee2);

		Employee example = new Employee();
		example.setName("test employee");
		example.setPosition(position);
		example.setSalary((int) (salary * 0.98));
		example.setEntryDate(LocalDateTime.now().toLocalDate().atStartOfDay().plusHours(1));
		example.setCompany(new Company(125, "Test Company and co."));
		
		List<Employee> foundEmployees = this.employeeService.findEmployeesByExample(example);
		System.out.println("EmployeeServiceIT > testFindEmployeesByExample() > foundEmployees.size(): " + foundEmployees.size());
		assertThat(foundEmployees.stream().map(Employee::getId).collect(Collectors.toList()))
				.containsExactly(employee1Id, employee2Id);
	}

}
