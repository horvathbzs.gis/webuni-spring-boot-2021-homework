package hu.webuni.horvathbzs.hr.service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;

import hu.webuni.horvathbzs.hr.config.HrConfigProperties;
import hu.webuni.horvathbzs.hr.config.HrConfigProperties.SmartRaise;
import hu.webuni.horvathbzs.hr.model.Employee;

public class SmartEmployeeService extends AbstractEmployeeService {

	@Autowired
	HrConfigProperties config;

	@Override
	public int getPayRaisePercent(Employee employee) {

		int smartPayRaisePercent;

		// --- calculate the years an employee spent in work ---
		// --- months of work needed to be calculated -> 2.5 years cannot be calculated
		// with ChronoUnit.YEARS ---
		double yearsInWork = (ChronoUnit.MONTHS.between(employee.getEntryDate(), LocalDateTime.now())) / 12.0;
		SmartRaise smartConfig = config.getSalary().getSmartRaise();
		
		TreeMap<Double, Integer> limits = smartConfig.getLimits();
		Entry<Double, Integer> floorEntry = limits.floorEntry(yearsInWork);
		
		smartPayRaisePercent = floorEntry == null ? 0 : floorEntry.getValue();
		
		System.out.println("SmartEmployeeService > getPayRaisePercent() > yearsInWork: " + yearsInWork);
		System.out
				.println("SmartEmployeeService > getPayRaisePercent() > smartPayRaisePercent: " + smartPayRaisePercent);

		return smartPayRaisePercent;
		
		
		/*
		// --- for iteration on Map ---
		Integer maxLimit = null;
		for(Entry<Double, Integer> limitEntry : smartConfig.getLimits().entrySet()) {
			if(yearsInWork > limitEntry.getKey())
				maxLimit = limitEntry.getValue();
			else
				break;
				
		//if years in work 0, than maxLimit will be null
		smartPayRaisePercent = maxLimit == null ? 0 : maxLimit;
		}
		*/
		
		/*
		// --- using stream ---
		Optional<Double> optinalMax = smartConfig.getLimits().keySet()
		.stream()
		.filter(k -> yearsInWork >= k)
		.max(Double :: compare);
		
		//if years in work 0, than maxLimit will be null
		smartPayRaisePercent = optinalMax.isEmpty() ? 0 : smartConfig.getLimits().get(optinalMax.get());
		*/	
	}

	/*
	public int getPayRaisePercent(Employee employee) {

		int smartPayRaisePercent;

		// --- calculate the years an employee spent in work ---
		// --- months of work needed to be calculated -> 2.5 years cannot be calculated
		// with ChronoUnit.YEARS ---
		double yearsInWork = (ChronoUnit.MONTHS.between(employee.getWorkStartDate(), LocalDateTime.now())) / 12.0;

		// --- get data from config ---
		double highWorkYears = config.getSalary().getSmartRaise().getHighWorkYears();
		int highPercent = config.getSalary().getSmartRaise().getHighPercent();
		double mediumWorkYears = config.getSalary().getSmartRaise().getMediumWorkYears();
		int mediumPercent = config.getSalary().getSmartRaise().getMediumPercent();
		double smallWorkYears = config.getSalary().getSmartRaise().getSmallWorkYears();
		int smallPercent = config.getSalary().getSmartRaise().getSmallPercent();

		// --- employee works for at least high years
		if (yearsInWork >= highWorkYears) {
			smartPayRaisePercent = highPercent;

			// --- employee works for at least medium years but less than high years
		} else if (yearsInWork < highWorkYears && yearsInWork >= mediumWorkYears) {
			smartPayRaisePercent = mediumPercent;

			// --- employee works for at least small years but less than medium years
		} else if (yearsInWork < mediumWorkYears && yearsInWork >= smallWorkYears) {
			smartPayRaisePercent = smallPercent;

			// --- employee works for less than small years
		} else {
			smartPayRaisePercent = 0;
		}

		System.out.println("SmartEmployeeService > getPayRaisePercent() > yearsInWork: " + yearsInWork);
		System.out
				.println("SmartEmployeeService > getPayRaisePercent() > smartPayRaisePercent: " + smartPayRaisePercent);

		return smartPayRaisePercent;
	}
	*/

	/*
	 * // --- No Configuration used ---
	 * 
	 * @Override public int getPayRaisePercent(Employee employee) {
	 * 
	 * int smartPayRaisePercent = 0;
	 * 
	 * // --- calculate the years an employee spent in work --- // --- NOTE:
	 * ChronoUnit.YEARS.between returns with whole years --- // --- e.g.
	 * between(2020-03-29, 2021-03-27) -> 0 years --- // --- months of work needed
	 * to be calculated -> 2.5 years cannot be calculated // with ChronoUnit.YEARS
	 * --- long monthsInWork =
	 * ChronoUnit.MONTHS.between(employee.getWorkStartDate(), LocalDateTime.now());
	 * double yearsInWork = (double) monthsInWork / 12.0;
	 * 
	 * 
	 * 
	 * // --- employee works for at least 10 years if (yearsInWork >= 10.0) {
	 * smartPayRaisePercent = 10;
	 * 
	 * // --- employee works for at least 5 years but less than 10 years } else if
	 * (yearsInWork < 10.0 && yearsInWork >= 5.0) { smartPayRaisePercent = 5;
	 * 
	 * // --- employee works for at least 2.5 years but less than 5 years } else if
	 * (yearsInWork < 5.0 && yearsInWork >= 2.5) { smartPayRaisePercent = 2;
	 * 
	 * // --- employee works for less than 2.5 years } else { smartPayRaisePercent =
	 * 0; }
	 * 
	 * System.out.
	 * println("SmartEmployeeService > getPayRaisePercent() > yearsInWork: " +
	 * yearsInWork); System.out.
	 * println("SmartEmployeeService > getPayRaisePercent() > smartPayRaisePercent: "
	 * + smartPayRaisePercent);
	 * 
	 * return smartPayRaisePercent; }
	 */
}
