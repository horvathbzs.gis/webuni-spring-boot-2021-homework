package hu.webuni.horvathbzs.hr.dto;


public class PositionCompanyAttributeDto {

	Long id;
	Long companyId;
	String companyName;
	Long positionId;
	String positionName;
	String expectedDegree;
	Integer lowerSalaryThreshold;
	
	public PositionCompanyAttributeDto() {
		
	}

	public PositionCompanyAttributeDto(Long id, Long companyId, String companyName, Long positionId,
			String positionName, String expectedDegree, Integer lowerSalaryThreshold) {
		super();
		this.id = id;
		this.companyId = companyId;
		this.companyName = companyName;
		this.positionId = positionId;
		this.positionName = positionName;
		this.expectedDegree = expectedDegree;
		this.lowerSalaryThreshold = lowerSalaryThreshold;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public String getExpectedDegree() {
		return expectedDegree;
	}

	public void setExpectedDegree(String expectedDegree) {
		this.expectedDegree = expectedDegree;
	}

	public Integer getLowerSalaryThreshold() {
		return lowerSalaryThreshold;
	}

	public void setLowerSalaryThreshold(Integer lowerSalaryThreshold) {
		this.lowerSalaryThreshold = lowerSalaryThreshold;
	}

	@Override
	public String toString() {
		return "PositionCompanyAttributeDto [id=" + id + ", companyId=" + companyId + ", companyName=" + companyName
				+ ", positionId=" + positionId + ", positionName=" + positionName + ", expectedDegree=" + expectedDegree
				+ ", lowerSalaryThreshold=" + lowerSalaryThreshold + "]";
	}
}
