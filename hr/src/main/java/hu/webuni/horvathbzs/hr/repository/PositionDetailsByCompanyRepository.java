package hu.webuni.horvathbzs.hr.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.webuni.horvathbzs.hr.model.PositionDetailsByCompany;

public interface PositionDetailsByCompanyRepository extends JpaRepository<PositionDetailsByCompany, Long> {
	
	List<PositionDetailsByCompany> findByPositionNameAndCompanyId(String positionName, long companyId);	
}
