package hu.webuni.horvathbzs.hr.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.webuni.horvathbzs.hr.model.Position;

public interface PositionRepository extends JpaRepository<Position, Integer> {

	public List<Position> findByName(String name);
}
