package hu.webuni.horvathbzs.hr.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class CompanyType {
	
	@Id
	@GeneratedValue
	private long id;
	
	private String name;
	
	public CompanyType() {
		super();
	}

	public CompanyType(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public CompanyType(String name) {
		super();
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
