package hu.webuni.horvathbzs.hr.mapper;

import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import hu.webuni.horvathbzs.hr.dto.CompanyDto;
import hu.webuni.horvathbzs.hr.dto.EmployeeDto;
import hu.webuni.horvathbzs.hr.model.Company;
import hu.webuni.horvathbzs.hr.model.Employee;


@Mapper(componentModel = "spring")
public interface CompanyMapper {

	CompanyDto companyToDto(Company company);
	
	@Mapping(target = "employees", ignore = true)
	@Named("summary")
	CompanyDto companySummaryToDto(Company company);

	List<CompanyDto> companiesToDtos(List<Company> companies);
	
	@IterableMapping(qualifiedByName = "summary")
	List<CompanyDto> companySummariesToDtos(List<Company> companies);

	Company dtoToCompany(CompanyDto companyDto);
	
	@Mapping(target = "companyName", source = "company.name")
	@Mapping(target = "title", source = "position.name")
	EmployeeDto employeeToDto(Employee employee);
	
}
