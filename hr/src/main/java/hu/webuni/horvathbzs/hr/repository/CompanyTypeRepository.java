package hu.webuni.horvathbzs.hr.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import hu.webuni.horvathbzs.hr.model.CompanyType;

public interface CompanyTypeRepository extends JpaRepository<CompanyType, Long> {

}
