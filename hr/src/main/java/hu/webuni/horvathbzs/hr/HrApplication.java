package hu.webuni.horvathbzs.hr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import hu.webuni.horvathbzs.hr.config.HrConfigProperties;
import hu.webuni.horvathbzs.hr.service.InitDbService;

@SpringBootApplication
public class HrApplication implements CommandLineRunner {

//	@Autowired
//	SalaryService salaryService;
	
	@Autowired
	HrConfigProperties config;
	
	@Autowired
	InitDbService initDbService;
	
	public static void main(String[] args) {
		SpringApplication.run(HrApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		
		initDbService.clearDb();
		initDbService.insertTestData();
		
//		SmartRaise smartConfig = config.getSalary().getSmartRaise();
//		for(Double limit : smartConfig.getLimits().keySet()) {
//			
//			int origSalary = 100;
//			LocalDateTime limitDay = LocalDateTime.now().minusDays((long)(limit * 365));
			
			//Employee e1 = new Employee(1L, "John", "developer", origSalary , limitDay.plusDays(1));
			//Employee e2 = new Employee(2L, "Martha", "consultant", origSalary , limitDay.minusDays(1));
			
			//salaryService.setEmployeeSalary(e1);
			//System.out.println();
			//salaryService.setEmployeeSalary(e2);
			
//		}
		
		/*
		Employee worksFor11Yrs = new Employee( (long) 1, "John", "developer",  100000, LocalDateTime.now().minus(Period.ofYears(11)));
		Employee worksFor6Yrs = new Employee( (long) 2, "Martha", "consultant",  10000, LocalDateTime.now().minus(Period.ofYears(6)));
		Employee worksFor3Yrs = new Employee( (long) 3, "Susan", "senior",  1000, LocalDateTime.now().minus(Period.ofYears(3)));
		Employee worksFor1Yrs = new Employee( (long) 3, "Peter", "junior",  100, LocalDateTime.now().minus(Period.ofYears(1)));
		
		salaryService.setEmployeeSalary(worksFor11Yrs);		
		salaryService.setEmployeeSalary(worksFor6Yrs);
		salaryService.setEmployeeSalary(worksFor3Yrs);	
		salaryService.setEmployeeSalary(worksFor1Yrs);
		*/
	}
}
