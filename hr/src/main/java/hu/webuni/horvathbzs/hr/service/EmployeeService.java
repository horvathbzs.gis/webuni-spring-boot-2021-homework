package hu.webuni.horvathbzs.hr.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import hu.webuni.horvathbzs.hr.model.Employee;

@Service
public interface EmployeeService {

	// --- the percent of Pay Raise based on spring.profile hr.salary config ---
	public int getPayRaisePercent(Employee employee);

	public List<Employee> findAll();

	public Optional<Employee> findById(long id);

	public Employee save(Employee employee);

	public Employee update(Employee employee);

	public void delete(long id);

	public List<Employee> findEmployeesByExample(Employee employee);
}
