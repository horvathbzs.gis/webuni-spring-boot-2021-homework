package hu.webuni.horvathbzs.hr.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.webuni.horvathbzs.hr.model.Company;
import hu.webuni.horvathbzs.hr.model.Employee;
import hu.webuni.horvathbzs.hr.repository.CompanyRepository;
import hu.webuni.horvathbzs.hr.repository.EmployeeRepository;

@Service
public class CompanyService {

	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	EmployeeRepository employRepository;

	@Autowired
	PositionService positionService;

	public List<Company> findAll() {
		return companyRepository.findAll();
	}

	public Optional<Company> findById(long id) {
		return companyRepository.findById(id);
	}

	public Company save(Company company) {
		return companyRepository.save(company);
	}

	@Transactional
	public Company update(Company company) {
		if (!companyRepository.existsById(company.getId()))
			return null;
		return companyRepository.save(company);
	}

	public void delete(long id) {
		companyRepository.deleteById(id);
	}

	@Transactional
	public Company addEmployee(long id, Employee employee) {
		Company company = companyRepository.findWithEmployeesById(id).get();
		positionService.setPositionByName(employee);
//		companyRepository.save(company); //cascade-del megy
		Employee employeeSaved = employeeRepository.save(employee);
		company.addEmployee(employeeSaved);
		return company;
	}

	/*
	 * This method does not deletes the employee from the database, only sets the
	 * company attribute to null, and removes the employee from the company's
	 * employeeList.
	 * 
	 */
	@Transactional
	public Company deleteEmployee(long id, long employeeId) {
		Company company = companyRepository.findById(id).get();
		Employee employee = employeeRepository.findById(employeeId).get();
		employee.setCompany(null);
		company.getEmployees().remove(employee);
//		employeeRepository.save(employee);
		return company;
	}

	@Transactional
	public Company replaceEmployees(long id, List<Employee> employees) {
		Company company = companyRepository.findById(id).get();
		company.getEmployees().stream().forEach(e -> {
			e.setCompany(null);
		});
		company.getEmployees().clear();

		for (Employee emp : employees) {
			company.addEmployee(emp);
			positionService.setPositionByName(emp);
			employeeRepository.save(emp);
		}
		return company;
	}
}
