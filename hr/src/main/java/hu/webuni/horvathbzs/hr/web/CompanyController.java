package hu.webuni.horvathbzs.hr.web;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import hu.webuni.horvathbzs.hr.dto.CompanyDto;
import hu.webuni.horvathbzs.hr.dto.EmployeeDto;
import hu.webuni.horvathbzs.hr.mapper.CompanyMapper;
import hu.webuni.horvathbzs.hr.mapper.EmployeeMapper;
import hu.webuni.horvathbzs.hr.model.AverageSalaryByPosition;
import hu.webuni.horvathbzs.hr.model.Company;
import hu.webuni.horvathbzs.hr.model.Employee;
import hu.webuni.horvathbzs.hr.repository.CompanyRepository;
import hu.webuni.horvathbzs.hr.service.CompanyService;
import hu.webuni.horvathbzs.hr.service.EmployeeService;

@RestController
@RequestMapping("api/companies")
public class CompanyController {

	@Autowired
	private EmployeeMapper employeeMapper;

	@Autowired
	private CompanyMapper companyMapper;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private CompanyRepository companyRepository;

	@GetMapping
	public List<CompanyDto> getAll(@RequestParam(required = false) Boolean full) {
		List<Company> companyList = null;
		Boolean notFull = (full == null || !full);
		System.out.println("CompanyController > getAll() > notFull: " + notFull);
		if (notFull) {
			companyList = companyService.findAll();
			System.out.println("CompanyController > getAll() > companyList.size(): " + companyList.size());
			return companyMapper.companySummariesToDtos(companyList);
		} else {
			companyList = companyRepository.findAllWithEmployees();
			System.out.println("CompanyController > getAll() > companyList.size(): " + companyList.size());
			return companyMapper.companiesToDtos(companyList);
		}
	}

	@GetMapping("/{id}")
	public CompanyDto getById(@PathVariable long id, @RequestParam(required = false) Boolean full) {
		System.out.println("CompanyController > getById() > full: " + full);
		System.out.println("CompanyController > getById() > id: " + id);
		Company company = companyService.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

		System.out.println("CompanyController > getById() > company.getEmployeeList().size(): "
				+ company.getEmployees().size());
		return full == null || full == false ? companyMapper.companySummaryToDto(company)
				: companyMapper.companyToDto(company);
	}

	@PostMapping
	public CompanyDto createCompany(@RequestBody CompanyDto companyDto) {
		return companyMapper.companyToDto(companyService.save(companyMapper.dtoToCompany(companyDto)));
	}

	@PutMapping("/{id}")
	public ResponseEntity<CompanyDto> modifyCompany(@PathVariable long id, @RequestBody CompanyDto companyDto) {
		companyDto.setId(id);
		Company updatedCompany = companyService.update(companyMapper.dtoToCompany(companyDto));
		if (updatedCompany == null) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(companyMapper.companyToDto(updatedCompany));
	}

	@DeleteMapping("/{id}")
	public void deleteCompany(@PathVariable long id) {
		companyService.delete(id);
	}

	@PostMapping("/{id}/employees")
	public CompanyDto addNewEmployee(@PathVariable long id, @RequestBody EmployeeDto employeeDto) {
		try {
			return companyMapper.companyToDto(companyService.addEmployee(id, employeeMapper.dtoToEmployee(employeeDto)));
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/{id}/employees/{employeeId}")
	public CompanyDto deleteEmployee(@PathVariable long id, @PathVariable long employeeId) {
		try {
			return companyMapper.companyToDto(companyService.deleteEmployee(id, employeeId));
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping("/{id}/employees")
	public CompanyDto replaceEmployees(@PathVariable long id, @RequestBody List<EmployeeDto> employeeList) {
		try {
			return companyMapper
					.companyToDto(companyService.replaceEmployees(id, employeeMapper.dtosToEmployees(employeeList)));
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(params = "aboveSalary")
	public List<CompanyDto> getCompaniesAboveASalary(@RequestParam int aboveSalary,
			@RequestParam(required = false) String full) {
		List<Company> companyAboveSalaryList = companyRepository.findByEmployeeWithSalaryHigherThan(aboveSalary);
		if (full == null || full.equals("false")) {
			return companyMapper.companySummariesToDtos(companyAboveSalaryList);
		} else
			return companyMapper.companiesToDtos(companyAboveSalaryList);
	}

	@GetMapping(params = "aboveEmployeeNumber")
	public List<CompanyDto> getCompaniesAboveEmployeeNumber(@RequestParam int aboveEmployeeNumber,
			@RequestParam(required = false) String full) {
		List<Company> companyAboveEmployeeNumberList = companyRepository
				.findByEmployeeCountHigherThan(aboveEmployeeNumber);
		if (full == null || full.equals("false")) {
			return companyMapper.companySummariesToDtos(companyAboveEmployeeNumberList);
		} else
			return companyMapper.companiesToDtos(companyAboveEmployeeNumberList);
	}

	@GetMapping("/{id}/salaryStats")
	public List<AverageSalaryByPosition> getSalaryStatsById(@PathVariable long id,
			@RequestParam(required = false) Boolean full) {
		return companyRepository.findAverageSalariesByPosition(id);
	}
}
