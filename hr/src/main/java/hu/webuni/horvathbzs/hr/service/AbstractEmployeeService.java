package hu.webuni.horvathbzs.hr.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import hu.webuni.horvathbzs.hr.model.Company;
import hu.webuni.horvathbzs.hr.model.Employee;
import hu.webuni.horvathbzs.hr.model.Position;
import hu.webuni.horvathbzs.hr.repository.EmployeeRepository;

public abstract class AbstractEmployeeService implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	PositionService positionService;
	
	@Override
	public List<Employee> findAll() {
		return employeeRepository.findAll();
	}

	@Override
	public Optional<Employee> findById(long id) {
		return employeeRepository.findById(id);
	}
	
	@Override
	@Transactional
	public Employee save(Employee employee) {
		clearCompanyAndSetPosition(employee);
		return employeeRepository.save(employee);
	}
	
	@Override
	@Transactional
	public Employee update(Employee employee) {
		if(!employeeRepository.existsById(employee.getId()))
			return null;
		clearCompanyAndSetPosition(employee);
		return employeeRepository.save(employee);
	}

	@Override
	public void delete(long id) {
		employeeRepository.deleteById(id);
	}
	
	public List<Employee> findEmployeesByExample(Employee example) {
		long id = example.getId();
		String name = example.getName();
		String title = null;
		Position position = example.getPosition();
		if(position != null)
			title = position.getName();
		int salary = example.getSalary();
		LocalDateTime entryDate = example.getEntryDate();
		String companyName = null;
		Company company = example.getCompany();
		if(company != null)
			companyName = company.getName();
		
		Specification<Employee> spec = Specification.where(null);
		
		if(id > 0)
			spec = spec.and(EmployeeSpecifications.hasId(id));
		if(StringUtils.hasText(title))
			spec = spec.and(EmployeeSpecifications.hasTitle(title));
		if(salary > 0)
			spec = spec.and(EmployeeSpecifications.hasSalary(salary));
		if(entryDate != null)
			spec = spec.and(EmployeeSpecifications.hasEntryDate(entryDate));
		if(StringUtils.hasText(companyName))
			spec = spec.and(EmployeeSpecifications.hasCompany(companyName));
		
		return employeeRepository.findAll(spec, Sort.by("id"));
	}
	
	private void clearCompanyAndSetPosition(Employee employee) {
		employee.setCompany(null);
		positionService.setPositionByName(employee);
	}
}
