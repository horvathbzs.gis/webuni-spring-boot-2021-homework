package hu.webuni.horvathbzs.hr.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.webuni.horvathbzs.hr.repository.CompanyTypeRepository;
import hu.webuni.horvathbzs.hr.model.Company;
import hu.webuni.horvathbzs.hr.model.CompanyType;
import hu.webuni.horvathbzs.hr.model.Position;
import hu.webuni.horvathbzs.hr.model.Qualification;
import hu.webuni.horvathbzs.hr.repository.CompanyRepository;
import hu.webuni.horvathbzs.hr.repository.EmployeeRepository;
import hu.webuni.horvathbzs.hr.repository.PositionDetailsByCompanyRepository;
import hu.webuni.horvathbzs.hr.repository.PositionRepository;

@Service
public class InitDbService {

	@Autowired
	CompanyTypeRepository companyTypeRepository;
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	PositionRepository positionRepository;
	@Autowired
	PositionDetailsByCompanyRepository positionCompanyAttributeRepository;
	@Autowired
	EmployeeRepository employeeRepository;

	public void clearDb() {
		employeeRepository.deleteAll();
		positionCompanyAttributeRepository.deleteAll();
		positionRepository.deleteAll();
		companyRepository.deleteAll();
	}

	@Transactional
	public void insertTestData() {

		// --- init Position ---
		List<Position> positionList = initPositionList();
		positionRepository.saveAll(positionList);
		positionList = positionRepository.findAll();

		Map<String, Position> positionMap = new HashMap<>();
		for (Position position : positionList) {
			positionMap.put(position.getName(), position);
		}

		// --- init Company Type ---
		List<CompanyType> types = initCompanyTypes();
		companyTypeRepository.saveAll(types);
		types = companyTypeRepository.findAll();

		Map<String, CompanyType> companyCategoryMap = new HashMap<>();
		for (CompanyType category : types) {
			companyCategoryMap.put(category.getName(), category);
		}

		// --- init Company ---
		List<Company> companyList = initCompanyList(companyCategoryMap);
		companyRepository.saveAll(companyList);
		companyList = companyRepository.findAll();
		System.out
				.println("InitDbService > insertTestData() > companyList.get(0).getId: " + companyList.get(0).getId());

		/*
		 * // --- init Employee --- List<Employee> employeeList =
		 * initEmployeeList(positionMap); employeeRepository.saveAll(employeeList); }
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * private List<Employee> initEmployeeList(Map<String, Position> positionMap) {
		 * List<Employee> employeeList = new ArrayList<>(); Employee e1 = new
		 * Employee("Takács Balázs", positionMap.get("könyvelő"), 300000,
		 * LocalDateTime.of(1999, 10, 5, 10, 0, 0)); employeeList.add(e1); Employee e2 =
		 * new Employee("Szetłak Karolina", positionMap.get("menedzser"), 600000,
		 * LocalDateTime.of(2005, 11, 19, 10, 0, 0)); employeeList.add(e2); Employee e3
		 * = new Employee("Müller Tamás", positionMap.get("fejlesztő"), 400000,
		 * LocalDateTime.of(2008, 2, 5, 10, 0, 0)); employeeList.add(e3);
		 * 
		 * Employee e4 = new Employee("Kovács Péter", positionMap.get("fejlesztő"),
		 * 300000, LocalDateTime.of(2007, 2, 23, 10, 0, 0)); employeeList.add(e4);
		 * Employee e5 = new Employee("Hideg Alíz", positionMap.get("menedzser"),
		 * 400000, LocalDateTime.of(2011, 8, 12, 10, 0, 0)); employeeList.add(e5);
		 * 
		 * return employeeList;
		 */
	}

	private List<Position> initPositionList() {
		List<Position> positionList = new ArrayList<>();
		Position p1 = new Position("menedzser", Qualification.UNIVERSITY);
		positionList.add(p1);
		Position p2 = new Position("fejlesztő", Qualification.COLLEGE);
		positionList.add(p2);
		Position p3 = new Position("könyvelő", Qualification.HIGH_SCHOOL);
		positionList.add(p3);

		return positionList;
	}

	private List<CompanyType> initCompanyTypes() {
		List<CompanyType> list = new ArrayList<>();
		CompanyType bt = new CompanyType("bt.");
		list.add(bt);
		CompanyType kft = new CompanyType("kft.");
		list.add(kft);
		CompanyType nyrt = new CompanyType("Nyrt.");
		list.add(nyrt);
		CompanyType zrt = new CompanyType("Zrt.");
		list.add(zrt);

		return list;
	}

	private List<Company> initCompanyList(Map<String, CompanyType> companyTypeMap) {
		List<Company> companyList = new ArrayList<>();
		Company c1 = new Company(125, "ABC Kft.", "Debrecen", companyTypeMap.get("kft."));
		companyList.add(c1);
		Company c2 = new Company(256, "DEF Rt.", "Budapest", companyTypeMap.get("Nyrt."));
		companyList.add(c2);

		return companyList;
	}
}
