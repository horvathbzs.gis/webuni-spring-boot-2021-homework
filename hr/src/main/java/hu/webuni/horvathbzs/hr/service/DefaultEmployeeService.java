package hu.webuni.horvathbzs.hr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.webuni.horvathbzs.hr.config.HrConfigProperties;
import hu.webuni.horvathbzs.hr.model.Employee;

@Service
public class DefaultEmployeeService extends AbstractEmployeeService {

	@Autowired
	HrConfigProperties config;
	
	@Override
	public int getPayRaisePercent(Employee employee) {
		
		//int defaultPayRaisePercent = 5;
		int defaultPayRaisePercent = config.getSalary().getDefaultRaise().getPercent();
		System.out.println("DefaultEmployeeService > getPayRaisePercent() > defaultPayRaisePercent: " + defaultPayRaisePercent);
		
		return defaultPayRaisePercent;
	}
}
