package hu.webuni.horvathbzs.hr.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import hu.webuni.horvathbzs.hr.model.Employee;
import hu.webuni.horvathbzs.hr.repository.EmployeeRepository;
import hu.webuni.horvathbzs.hr.repository.PositionDetailsByCompanyRepository;
import hu.webuni.horvathbzs.hr.repository.PositionRepository;

@Service
public class SalaryService {
	
	private EmployeeService employeeService;
	private PositionRepository positionRepository;
	PositionDetailsByCompanyRepository positionDetailsByCompanyRepository;

	EmployeeRepository employeeRepository;
	
	public SalaryService(EmployeeService employeeService, PositionRepository positionRepository,
			PositionDetailsByCompanyRepository positionDetailsByCompanyRepository,
			EmployeeRepository employeeRepository) {
		super();
		this.employeeService = employeeService;
		this.positionRepository = positionRepository;
		this.positionDetailsByCompanyRepository = positionDetailsByCompanyRepository;
		this.employeeRepository = employeeRepository;
	}

	public void setEmployeeSalary(Employee employee) {		
		employee.setSalary( employee.getSalary() + 
				(int) (employee.getSalary() * employeeService.getPayRaisePercent(employee) / 100) );
	}
	
	@Transactional
	public void raiseMinimalSalary(String positionName, int minSalary, long companyId) {
		positionDetailsByCompanyRepository.findByPositionNameAndCompanyId(positionName, companyId)
		.forEach(pd ->{
			pd.setMinSalary(minSalary);
//			pd.getCompany().getEmployees().forEach(e ->{
//				if(e.getPosition().getName().equals(positionName)
//						&& e.getSalary() < minSalary)
//					e.setSalary(minSalary);
//			});
		});
		
		employeeRepository.updateSalaries(positionName, minSalary, companyId);
	}
}
