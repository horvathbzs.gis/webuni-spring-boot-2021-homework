package hu.webuni.horvathbzs.hr.model;


/**
 * Interface for Query
 * 
 * 
 */
public interface AverageSalaryByPosition {

	public String getPosition();
	public int getAverageSalary();
}
