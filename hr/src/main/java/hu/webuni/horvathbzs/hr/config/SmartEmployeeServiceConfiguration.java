package hu.webuni.horvathbzs.hr.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import hu.webuni.horvathbzs.hr.service.EmployeeService;
import hu.webuni.horvathbzs.hr.service.SmartEmployeeService;

@Configuration
@Profile("smart")
public class SmartEmployeeServiceConfiguration {

	@Bean
	public EmployeeService employeeService() {
		return new SmartEmployeeService();
	}
}
