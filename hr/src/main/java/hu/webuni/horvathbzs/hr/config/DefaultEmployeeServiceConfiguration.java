package hu.webuni.horvathbzs.hr.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import hu.webuni.horvathbzs.hr.service.DefaultEmployeeService;
import hu.webuni.horvathbzs.hr.service.EmployeeService;

@Configuration
@Profile("!smart")
public class DefaultEmployeeServiceConfiguration {

	@Bean
	public EmployeeService employeeService() {
		return new DefaultEmployeeService();
	}
}
