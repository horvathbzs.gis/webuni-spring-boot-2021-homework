package hu.webuni.horvathbzs.hr.model;

public enum Qualification {
	HIGH_SCHOOL, COLLEGE, UNIVERSITY, PHD;
}
