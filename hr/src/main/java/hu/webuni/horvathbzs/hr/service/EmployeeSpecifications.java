package hu.webuni.horvathbzs.hr.service;

import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.data.jpa.domain.Specification;

import hu.webuni.horvathbzs.hr.model.Company_;
import hu.webuni.horvathbzs.hr.model.Employee;
import hu.webuni.horvathbzs.hr.model.Employee_;
import hu.webuni.horvathbzs.hr.model.Position_;

public class EmployeeSpecifications {

	public static Specification<Employee> hasId(long id) {
		return (root, cq, cb) -> cb.equal(root.get(Employee_.id), id);
	}

	/*
	 * Name matches with the beginning, case-insensitive
	 * 
	 */
	public static Specification<Employee> hasName(String name) {
		return (root, cq, cb) -> cb.like(cb.lower(root.get(Employee_.name)), (name + "%").toLowerCase());
	}

	/*
	 * Title matches with employee.position.name
	 * 
	 * @param title String of a position name
	 */
	public static Specification<Employee> hasTitle(String title) {
		return (root, cq, cb) -> cb.equal(root.get(Employee_.position).get(Position_.name), title);
	}

	/*
	 * Salary matches between +/- 5 percents of the employee's salary
	 */
	public static Specification<Employee> hasSalary(int salary) {
		int minus5percent = (int) (salary * 0.95);
		int plus5percent = (int) (salary * 1.05);
		return (root, cq, cb) -> cb.between(root.get(Employee_.salary), minus5percent, plus5percent);
	}

	/*
	 * EntryDateTime matches the entry DAY
	 */
	public static Specification<Employee> hasEntryDate(LocalDateTime entryDateTime) {
		LocalDateTime startOfDay = LocalDateTime.of(entryDateTime.toLocalDate(), LocalTime.of(0, 0));
		return (root, cq, cb) -> cb.between(root.get(Employee_.entryDate), startOfDay, startOfDay.plusDays(1));
	}

	/*
	 * Company name matches with the beginning of an employee's company name,
	 * case-insensitive
	 * 
	 */
	public static Specification<Employee> hasCompany(String companyName) {
		return (root, cq, cb) -> cb.like(cb.lower(root.get(Employee_.company).get(Company_.name)),
				(companyName + "%").toLowerCase());
	}

}
