package hu.webuni.horvathbzs.hr.config;

import java.time.Duration;
import java.util.TreeMap;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "hr")
@Component
public class HrConfigProperties {

	private Salary salary = new Salary();

	public Salary getSalary() {
		return salary;
	}

	public void setSalary(Salary salary) {
		this.salary = salary;
	}

	public static class Salary {

		private DefaultRaise defaultRaise = new DefaultRaise();
		private SmartRaise smartRaise = new SmartRaise();

		public DefaultRaise getDefaultRaise() {
			return defaultRaise;
		}

		public void setDefaultRaise(DefaultRaise defaultRaise) {
			this.defaultRaise = defaultRaise;
		}

		public SmartRaise getSmartRaise() {
			return smartRaise;
		}

		public void setSmartRaise(SmartRaise smartRaise) {
			this.smartRaise = smartRaise;
		}
	}

	public static class DefaultRaise {
		private int percent;

		public int getPercent() {
			return percent;
		}

		public void setPercent(int percent) {
			this.percent = percent;
		}
	}
	
	public static class SmartRaise {
		
		// Key: years, Value: percent of salary raise
		private TreeMap<Double, Integer> limits;
		
		public TreeMap<Double, Integer> getLimits() {
			return limits;
		}
		
		public void setLimits(TreeMap<Double, Integer> limits) {
			this.limits = limits;
		}
	}
	
private JwtData jwt = new JwtData();
	
	public static class JwtData{
		
		private String issuer;
		private String secret;
		private String alg;
		private Duration duration;
		
		public String getIssuer() {
			return issuer;
		}
		public void setIssuer(String issuer) {
			this.issuer = issuer;
		}
		public String getSecret() {
			return secret;
		}
		public void setSecret(String secret) {
			this.secret = secret;
		}
		public String getAlg() {
			return alg;
		}
		public void setAlg(String alg) {
			this.alg = alg;
		}
		public Duration getDuration() {
			return duration;
		}
		public void setDuration(Duration duration) {
			this.duration = duration;
		}
	}
	
	public JwtData getJwt() {
		return jwt;
	}

	public void setJwt(JwtData jwt) {
		this.jwt = jwt;
	}

}
